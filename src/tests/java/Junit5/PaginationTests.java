package Junit5;

import core.SetUp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import pageobjects.CategoryMenuUI;
import pageobjects.PaginationUI;

public class PaginationTests {
    public ChromeDriver driver;
    public SetUp setUp;
    public CategoryMenuUI catMenuUI;
    public PaginationUI pagination;
    @Before
    public void startTest(){
        driver = new ChromeDriver();
        catMenuUI = new CategoryMenuUI(driver);
        pagination = new PaginationUI(driver);
        setUp = new SetUp(driver);
        setUp.openNews();
    }

    @Test
    public void checkPaginationElements() {
        int actual = catMenuUI.checkCategories(driver, pagination.paginationElementsList);
        int expected = 5;
        Assert.assertEquals(expected, actual);
    }
    @Test
    public void checkPagination() {
        pagination.checkNewsLoading(driver, pagination.three, pagination.message);
        int actual = catMenuUI.checkElement(driver, pagination.message);
        int expected = 1;
        Assert.assertEquals(expected, actual);
    }


    @After
    public void after() {
        SetUp.quitBrowser();
    }
}
