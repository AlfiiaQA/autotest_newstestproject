package Junit5;

import core.SetUp;
import org.junit.*;
import org.openqa.selenium.chrome.ChromeDriver;
import pageobjects.NewsCardsBlockUI;


import static pageobjects.NewsCardsBlockUI.*;


public class NewsCardsBlockTests {
    public ChromeDriver driver;
    public SetUp setUp;
    public NewsCardsBlockUI newsCardsBlockUI;
    @Before
    public void startTest() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\admin\\Downloads\\chromedriver.exe");
        driver = new ChromeDriver();
        setUp = new SetUp(driver);
        newsCardsBlockUI = new NewsCardsBlockUI(driver);
        setUp.openNews();
    }
    @Test
    public void NCB1_CheckResours () {
        int actual=newsCardsBlockUI.CheckElement(driver,newsCardsResoursID);
        int expected =1;
        Assert.assertEquals(expected,actual);
    }
    @Test
    public void NCB2_CheckTitle () {
        int actual=newsCardsBlockUI.CheckElement(driver,newsCardsTitleID);
        int expected =1;
        Assert.assertEquals(expected,actual);
    }
    @Test
    public void NCB3_CheckDescription () {
        int actual=newsCardsBlockUI.CheckElement(driver,newsCardsDescriptionID);
        int expected =1;
        Assert.assertEquals(expected,actual);
    }
    @Test
    public void NCB4_CheckImage () {
        int actual=newsCardsBlockUI.CheckElement(driver,newsCardsImageID);
        int expected =1;
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void NCB5_CheckAuthor  () {
        int actual=newsCardsBlockUI.CheckElement(driver,newsCardsAuthorID);
        int expected =1;
        Assert.assertEquals(expected,actual);
    }



    @After
    public void EndTest(){
        driver.quit();
    }


}
