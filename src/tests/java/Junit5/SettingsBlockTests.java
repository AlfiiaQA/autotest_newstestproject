package Junit5;

import core.SetUp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pageobjects.SettingsBlockUI;

public class SettingsBlockTests {
    @Before
    public void before() {
        SetUp.openNews();
    }
    public SettingsBlockUI settingsBlock;

    @Test
    public void sortNewsByDate() {
        settingsBlock.sortNewsByDate();
        Assert.assertEquals("18-05-2020", settingsBlock.date);
    }

    @After
    public void after() {
        SetUp.quitBrowser();
    }
}
