package Junit5;

import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonBooleanFormatVisitor;
import core.SetUp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import pageobjects.CategoryMenuUI;


public class CategoryMenuTests {
    public ChromeDriver driver;
    public SetUp setUp;
    public CategoryMenuUI catMenuUI;

    @Before
    public void startTest(){
        driver = new ChromeDriver();
        catMenuUI = new CategoryMenuUI(driver);
        setUp = new SetUp(driver);
        setUp.openNews();
    }

    @Test
    public void checkDefaultCategories() {
        int actual = catMenuUI.checkCategories(driver, catMenuUI.defaultCategoryList);
        int expected = 8;
        Assert.assertEquals(expected, actual);
    }
    @Test
    public void checkAddButton() {
        int actual = catMenuUI.checkElement(driver, catMenuUI.addCategoryButtonID);
        int expected = 1;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void checkCustomCategory() {
        catMenuUI.addCustomCategories(driver, catMenuUI.firstCustomCategory);
        catMenuUI.addCustomCategories(driver, catMenuUI.secondCustomCategory);

        int expected = 2;
        int actual = catMenuUI.checkCategories(driver, catMenuUI.customCategoryList);
        Assert.assertEquals(expected, actual);
    }
    @Test
    public void checkButtonDisappear() {
        catMenuUI.addCustomCategories(driver, catMenuUI.firstCustomCategory);
        catMenuUI.addCustomCategories(driver, catMenuUI.secondCustomCategory);
        var checkButton = driver.findElement(catMenuUI.body);
        Assert.assertFalse(checkButton.getText().contains("Add news category"));
    }



    @After
    public void after() {
        SetUp.quitBrowser();
    }
}
