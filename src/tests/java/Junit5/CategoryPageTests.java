package Junit5;

import core.SetUp;
import org.junit.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import pageobjects.CategoryPageUI;
import static pageobjects.CategoryPageUI.*;



public class CategoryPageTests  {
    public ChromeDriver driver;
    public SetUp setUp;
    public CategoryPageUI categoryPageUI;
   @Before
          public void startTest(){
       System.setProperty("webdriver.chrome.driver","C:\\Users\\admin\\Downloads\\chromedriver.exe");
       driver = new ChromeDriver();
       setUp = new SetUp(driver);
       categoryPageUI= new CategoryPageUI(driver) ;
       setUp.openCat();
   }

    @Test
    public void CP1_OpenCategoryPage ()    {

        var actualElement = driver.findElement(body);
        Assert.assertTrue(actualElement.getText().contains("CHOOSE A NEWS CATEGORY"));
    }
    @Test
    public void CP2_GoToNewsPage()    {
      WebElement category = driver.findElement(firstNPButtonID);
      category.click();
        var actualElement = driver.findElement(body);

      Assert.assertTrue(actualElement.getText().contains("Add Topic"));

    }
    @Test
    public void CP3_CheckDefaultСategories  ()    {

        int actual=categoryPageUI.CheckСategories(categoryPageUI.defaultCategoryList);
        int expected =8;
        Assert.assertEquals(expected,actual);
   }
    @Test
    public void CP4_CheckAddTopicButton()    {

        int actual=categoryPageUI.CheckElement(driver,addNPButtonID);
        int expected =1;
        Assert.assertEquals(expected,actual);
    }
    @Test
    public void CP5_CheckWorkAddTopicButton()    {
       categoryPageUI.createCustomCategory(driver,custom1);
        categoryPageUI.createCustomCategory(driver,custom2);
        int actual=categoryPageUI.CheckСategories(categoryPageUI.customCategoryList);
        int expected =2;
        Assert.assertEquals(expected,actual);
    }
    @Test
    public void CP6_WithLogoGoToNewsPage() {
        WebElement logo = driver.findElement(logoNPButtonID);
        logo.click();
        var actualElement = driver.findElement(body);

        Assert.assertTrue(actualElement.getText().contains("Add Topic"));
    }
    @After
    public void EndTest(){
driver.quit();
}
}

