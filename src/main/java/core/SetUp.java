package core;

import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class SetUp {
    public static void setProperty(){
        System.setProperty("webdriver.chrome.driver", "/home/alfiia/Downloads/chromedriver");
    }
    public SetUp(ChromeDriver driver){
        this.driver = driver;
    }
    private static ChromeDriver driver;
    static UrlPages pages = new UrlPages();
    
    public void openCat() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(pages.baseUrl+pages.catPage);
    }
    
    public static void openNews() {
        driver.get(pages.baseUrl+pages.newsPage);
    }
    public static void quitBrowser() {driver.quit();}
}
