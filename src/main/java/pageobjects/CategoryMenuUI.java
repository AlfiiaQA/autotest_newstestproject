package pageobjects;

import modals.ModalNewsPageUI;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CategoryMenuUI {
    public CategoryMenuUI(ChromeDriver driver){
        this.driver = driver;
    }

    private static final By firstCatButtonID =  By.xpath("//a[@data-at='at-category-sport-news']");
    private static final By secondCatButtonID = By.xpath("//a[@data-at='at-category-coronavirus-news']");
    private static final By thirdCatButtonID = By.xpath("//a[@data-at='at-category-trump-news']");
    private static final By fourthCatButtonID = By.xpath("//a[@data-at='at-category-health-news']");
    private static final By fifthCatButtonID = By.xpath("//a[@data-at='at-category-food-news']");
    private static final By sixthCatButtonID = By.xpath("//a[@data-at='at-category-bitcoin-news']");
    private static final By seventhCatButtonID = By.xpath("//a[@data-at='at-category-science-news']");
    private static final By eighthCatButtonID = By.xpath("//a[@data-at='at-category-travel-news']");
    public static String firstCustomCategory = "music";
    public static String secondCustomCategory ="movies";
    public static final By ninthCategoryButtonID = By.xpath("//a[@data-at='data-at-"+firstCustomCategory+"-category-news']");
    private static final By tenthCategoryButtonID = By.xpath("//a[@data-at='data-at-"+secondCustomCategory+"-category-news']");
    public static final By addCategoryButtonID = By.xpath("//a[@data-at='at-news-category-add-button']");
    public static final By body = By.tagName("body");
    public ChromeDriver driver;
    ModalNewsPageUI catmodalnews;

    public By[] defaultCategoryList = new By[] {
        firstCatButtonID,
        secondCatButtonID,
        thirdCatButtonID,
        fourthCatButtonID,
        fifthCatButtonID,
        sixthCatButtonID,
        seventhCatButtonID,
        eighthCatButtonID
    };
    public By[] customCategoryList = new By[] {
            ninthCategoryButtonID,
            tenthCategoryButtonID
    };

    public int checkElement(ChromeDriver driver, By element) {
        this.driver = driver;
        By actualElement=element;
        int count = 0;
        var webelement = driver.findElement(actualElement);
        if(webelement!=null){
            count++;
        }
        return count;
    }

    public int checkCategories(ChromeDriver driver, By [] categoriesList) {
        int count=0;
        for(int i=0;i<=categoriesList.length-1;i++) {
            if (checkElement(driver,categoriesList[i])==1){
                count++;
            };
        }
        return count;
    }
    public void addCustomCategories(ChromeDriver driver, String customCategory) {
        String newCustom = customCategory;
        WebElement addButton = driver.findElement(addCategoryButtonID);
        addButton.click();
        WebElement fillInput = driver.findElement(catmodalnews.inputID);
        fillInput.sendKeys(newCustom);
        WebElement acceptButton = driver.findElement(catmodalnews.acceptButtonID);
        acceptButton.click();
    }

}


