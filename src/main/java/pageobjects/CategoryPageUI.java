package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CategoryPageUI {
        private static ChromeDriver driver;
        public CategoryPageUI(ChromeDriver driver) {
                this.driver = driver;
        }

        public static String custom1 = "naruto";
        public static String custom2 = "boruto";
        public static final By logoNPButtonID = By.xpath("//img[@data-at='logo']");
        public static final By firstNPButtonID = By.xpath("//div[@data-at='at-category-sport']");
        private static final By secondNPButtonID = By.xpath("//div[@data-at='at-category-coronavirus']");
        private static final By thirdNPButtonID = By.xpath("//div[@data-at='at-category-trump']");
        private static final By fourthNPButtonID = By.xpath("//div[@data-at='at-category-health']");
        private static final By fifthNPButtonID = By.xpath("//div[@data-at='at-category-food']");
        private static final By sixthNPButtonID = By.xpath("//div[@data-at='at-category-bitcoin']");
        private static final By seventhNPButtonID = By.xpath("//div[@data-at='at-category-science']");
        private static final By eighthNPButtonID = By.xpath("//div[@data-at='at-category-travel']");
        private static final By custom1NPButtonID = By.xpath("//div[@data-at='data-at-" + custom1 + "-category']");
        private static final By custom2NPButtonID = By.xpath("//div[@data-at='data-at-" + custom2 + "-category']");
        public static final By addNPButtonID = By.xpath("//button[@data-at='at-category-add-button']");
        public static final By addCategoryField = By.xpath("//input[@placeholder='Add Topic']");
        public static final By addCategoryButton = By.xpath(" //div[@class='ui green inverted right attached button']");
        public static final By body = By.tagName("body");

        public By[] defaultCategoryList = new By[]{firstNPButtonID, secondNPButtonID, thirdNPButtonID, fourthNPButtonID, fifthNPButtonID, sixthNPButtonID, seventhNPButtonID, eighthNPButtonID};
        public By[] customCategoryList = new By[]{custom1NPButtonID, custom2NPButtonID};

        public int CheckElement(ChromeDriver driver, By element) {

                this.driver = driver;
                By actualElement = element;
                int count = 0;
                var searchElement = driver.findElement(actualElement);
                if (searchElement != null) {
                        count++;
                }
                return count;
        }



        public int CheckСategories(By[] categotyList) {
                By[] list = categotyList;
                int count = 0;
                for (int i = 0; i <= list.length - 1; i++) {
                        if (CheckElement(driver, list[i]) == 1) {
                                count++;
                        }
                        ;
                }
                return count;
        }

        public void createCustomCategory(ChromeDriver driver, String custom) {
                String newCustom = custom;

                WebElement addButton = driver.findElement(addNPButtonID);
                addButton.click();
                WebElement categoryField = driver.findElement(addCategoryField);
                categoryField.sendKeys(newCustom);
                WebElement greenButton = driver.findElement(addCategoryButton);
                greenButton.click();
        }
}





