package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class SettingsBlockUI {
    private static final By dataPickerFromID =  By.xpath("[data-at='data-at-date-picker-from']");
    private static final By dataPickerToID = By.xpath("[data-at='data-at-date-picker-to']");
    private static final By filterButtonID = By.xpath("[data-at='data-at--button']");
    private static final By firstLangButtonID = By.xpath("[data-at='data-at-english-language-button']");
    private static final By secondLangButtonID =  By.xpath("[data-at='data-at-deutschland-language-button']");
    private static final By thirdLangButtonID = By.xpath("[data-at='data-at-france-language-button']");
    private static final By fourthLangButtonID = By.xpath("[data-at='data-at-spain-language-button']");
    private static final By fifthLangButtonID = By.xpath("[data-at='data-at-russian-language-button']");
    public static boolean date;

    public ChromeDriver driver;
    public void sortNewsByDate() {
        driver.findElement(dataPickerFromID).clear();
        driver.findElement(dataPickerFromID).sendKeys("10-05-2020");
        driver.findElement(dataPickerToID).clear();
        driver.findElement(dataPickerToID).sendKeys("18-05-2020");
        driver.findElement(filterButtonID).click();
        date = driver.findElement(NewsCardsBlockUI.newsCardsDateID).getText().contains("18-05-2020");
    }
    //API
    public void sortNewsByFirstLanguage() {
        driver.findElement(firstLangButtonID).click();
    }
    public void sortNewsBySecondLanguage() {
        driver.findElement(secondLangButtonID).click();
    }
    public void sortNewsByThirdLanguage() {
        driver.findElement(thirdLangButtonID).click();
    }
    public void sortNewsByFourthLanguage() {
        driver.findElement(fourthLangButtonID).click();
    }
    public void sortNewsByFifthLanguage() {
        driver.findElement(fifthLangButtonID).click();
    }

}
