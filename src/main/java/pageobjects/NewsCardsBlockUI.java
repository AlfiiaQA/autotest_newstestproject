package pageobjects;
import org.openqa.selenium.By;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class NewsCardsBlockUI {
    private static ChromeDriver driver;
    public NewsCardsBlockUI(ChromeDriver driver) {
        this.driver = driver;
    }

    public static final By newsCardsImageID =  By.xpath("//div[@data-at='at-newscard-image']");
    public static final By newsCardsResoursID =  By.xpath("//a[@data-at='at-newscard-source-link']");
    public static final By newsCardsTitleID = By.xpath("//a[@data-at='at-newscard-title']");
    public static final By newsCardsDescriptionID = By.xpath("//div[@data-at='at-newscard-description']");
    public static final By newsCardsAuthorID =  By.xpath("//div[@data-at='at-newscard-Author']");


    public int CheckElement(ChromeDriver driver, By element) {

        this.driver = driver;
        By actualElement = element;
        int count = 0;
        var searchElement = driver.findElement(actualElement);
        if (searchElement != null) {
            count++;
        }
        return count;
    }


}
