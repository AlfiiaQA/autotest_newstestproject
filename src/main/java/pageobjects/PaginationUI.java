package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class PaginationUI {
    public PaginationUI(ChromeDriver driver){
        this.driver = driver;
    }
    private static final By goToStartButtonID =  By.xpath("//a[@aria-label='First item']");
    private static final By goToEndButtonID = By.xpath("//a[@aria-label='Last item']");
    private static final By goToPreviousButtonID = By.xpath("//a[@aria-label='Previous item']");
    private static final By goToNextButtonID = By.xpath("//a[@aria-label='Next item']");
    private static final By firstPageButtonID = By.xpath("//*[@id=\"__next\"]/div/div[2]/div[3]/div/a[2]");
    public static final By three = By.xpath("//a[@value='3']");
    public static final By message = By.xpath("//div[@class='Toastify__toast Toastify__toast--warning']");

    public ChromeDriver driver;

    public By [] paginationElementsList = new By[]  {
        goToStartButtonID,
        goToEndButtonID,
        goToPreviousButtonID,
        goToNextButtonID,
        firstPageButtonID
    };
    public void checkNewsLoading(ChromeDriver driver, By firstElement, By secondElement) {
        WebElement thirdPage = driver.findElement(firstElement);
        thirdPage.click();
        WebDriverWait wait = new WebDriverWait(driver,3);
        wait.until(ExpectedConditions.visibilityOfElementLocated(secondElement));
     }



}

