package modals;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class BaseModal {
    public static By modalWindowID;
    public static By titleID;
    public static By inputID;
    public static By closeButtonID;
    public static By acceptButtonID;
    public static By cancelButtonID;
    public ChromeDriver driver;

    public abstract void open();
    public void close() {
        driver.findElement(closeButtonID).click();
    }
    public void accept() {
        driver.findElement(acceptButtonID).click();
    }
    public void cancel() {
        driver.findElement(cancelButtonID).click();
    }
}