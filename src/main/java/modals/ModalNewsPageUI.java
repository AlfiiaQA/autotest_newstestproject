package modals;

import core.SetUp;
import org.openqa.selenium.By;
import pageobjects.CategoryMenuUI;

public class ModalNewsPageUI extends BaseModal {
    private static final By modalWindowID = By.xpath("[data-at='news-page-modal-window']");
    private static final By titleID = By.xpath("[data-at='news-page-modal-title']");
    public static final By inputID = By.xpath("//input[@placeholder='Add Topic']");
    private static final By closeButtonID = By.xpath("[data-at='news-page-modal-close-button']");
    public static final By acceptButtonID = By.xpath("//div[@class='ui green inverted right attached button']");
    private static final By cancelButtonID = By.xpath("[data-at='news-page-modal-accept-button']");
    public SetUp setup;
    public CategoryMenuUI catmenu;

    @Override
    public void open() {
        setup.openNews();
        driver.findElement(catmenu.addCategoryButtonID).click();
    }

    @Override
    public void close() {
        super.close();
    }

    @Override
    public void accept() {
        super.accept();
    }

    @Override
    public void cancel() {
        super.cancel();
    }
}
