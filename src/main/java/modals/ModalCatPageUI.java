package modals;

import core.SetUp;
import org.openqa.selenium.By;

public class ModalCatPageUI extends BaseModal{
    private static final By modalWindowID = By.xpath("[data-at='cat-page-modal-window']");
    private static final By titleID = By.xpath("[data-at='cat-page-modal-title']");
    public static final By inputID = By.xpath("[data-at='cat-page-modal-input']");
    private static final By closeButtonID = By.xpath("[data-at='cat-page-modal-close-button']");
    public static final By acceptButtonID = By.xpath("[data-at='cat-page-modal-accept-button']");
    private static final By cancelButtonID = By.xpath("[data-at='cat-page-modal-accept-button']");
    public SetUp setup;

    @Override
    public void open() {
        setup.openCat();
    }

    @Override
    public void close() {
        super.close();
    }

    @Override
    public void accept() {
        super.accept();
    }

    @Override
    public void cancel() {
        super.cancel();
    }
}
